rm(list=ls())
library(tune)
library(workflows)
library(tidymodels)
library(tictoc)
library(DESeq2)
library(phyloseq)
library(dplyr)
library(yingtools2)
library(data.table)
library(ifultools)
library(stringr)
library(ggplot2)
library(gtools)
library(tidyr)
library(ggprism)
library(ranger)
library(caret)

source('./ml_model_class.R')

# Create a directory to save figures and tables
mainDir <- "../../results"
subDir <- "RF_final_Sev_aggregate_taxonomy_object_oriented"
dir.create(file.path(mainDir, subDir,Sys.Date()), showWarnings = TRUE,recursive = TRUE)
fig_folder <- paste(mainDir,subDir,Sys.Date(),sep="/")


phylum_col <- c("Firmicutes"= "#9C854E",
                "Bacteroidetes" = "#51AB9B",
                "Actinobacteria" = "#A77097",
                "Proteobacteria" = "red",
                "Verrucomicrobia" = "#33445e",
                "Synergistetes" = "#e2eab7",
                "Fusobacteria"= "#653131",
                "Euryarchaeota" = "#8c8c8c")


#mod <- rf_model
plot_importance <- function(mod){
  library(ggthemes)
  library(ggprism)
  varimp <- mod$variable.importance
  dat <- data.frame(variable=names(varimp),importance=varimp)
  dat$variable <- gsub("\\_"," ", dat$variable)
  dat$variable <- gsub("X\\."," ", dat$variable)
  dat$variable <- gsub("\\.\\."," ", dat$variable)
  dat$variable <- gsub("\\."," ", dat$variable)
  dat <-  dat[dat$importance>0,]
  dat <-  dat[order(dat$importance,decreasing = T),]
  ggplot(dat, aes(x=reorder(variable,importance), y=importance))+
    geom_bar(stat="identity", position="dodge", color = "black",fill="orange")+ coord_flip()+
    ylab("Variable Importance")+
    theme_prism()+
    xlab("Variables")
}

get_vst_data <- function(phy, o_relative = F, o_glom = F, glom_level = NULL){
  # not run:
  # uncomment for debug:
  #phy <- phy_tng
  #o_relative <- T
  #o_glom <- T
  #glom_level <- "Genus"
  
  ps_stl <- phy
  ps_stl <- subset_samples(ps_stl,Severity %in% c("severe","moderate"))
  ps_stl <-  prune_taxa(taxa_sums(ps_stl)>0,ps_stl)
  # Total number of samples:
  nsamples(ps_stl)
  stl_sm_dt <- data.frame(sample_data(ps_stl))
  # Now lets focus on the stool samples:
  ps_stl
  
  ### Compute Diversity
  stl_dt <- get.samp(ps_stl,stats = T) %>% as.data.frame()
  phy_deseq <- ps_stl 
  
  prevdf = apply(X = otu_table(phy_deseq),
                 MARGIN = ifelse(taxa_are_rows(phy_deseq), yes = 1, no = 2),
                 FUN = function(x){sum(x > 0)})
  # Add taxonomy and total read counts to this data.frame
  prevdf = data.frame(Prevalence = prevdf,
                      TotalAbundance = taxa_sums(phy_deseq),
                      tax_table(phy_deseq)@.Data)
  prevdf$OTU <-  rownames(prevdf)
  prevdf <- prevdf[order(prevdf$Prevalence,decreasing = T), ]
  prevdf$OTU <- factor(as.character(prevdf$OTU) , levels = as.character(prevdf$OTU))
  prevdf$Prop <- prevdf$TotalAbundance/sum(prevdf$TotalAbundance)
  
  prev_frac<-0.05
  prev_cutoff <- prev_frac*nsamples(phy_deseq) # Cut-off
  ab_cutoff <- 1e-4# Cut-off
  # Prevalence
  prevdf_fil <- prevdf[prevdf$Prevalence >= prev_cutoff, ]
  # Abundance
  prevdf_fil <- prevdf_fil[prevdf_fil$Prop >= ab_cutoff, ]
  most_prevalent_taxa<-unique(rownames(prevdf_fil))
  most_prevalent_taxa
  
  phy_fil <- prune_taxa(most_prevalent_taxa,phy_deseq)

  if (o_relative == F){
    dds <- phyloseq_to_deseq2(phy_fil, ~ Severity) #replace this with any sample variable(s)
    dds <- estimateSizeFactors(dds,"poscounts")
    dds <- estimateDispersions(dds)
    dds <- DESeq(dds,fitType= "local")
    
    vst_dt <- getVarianceStabilizedData(dds)
    dt_meta <-  data.frame(sample_data(phy_fil))
    dt_meta$SampleID  <- rownames(dt_meta)
    dt_meta <- dt_meta[match(colnames(vst_dt),dt_meta$SampleID),]
    
    #make phyloseq object 
    dt_meta$sample  <- rownames(dt_meta)
    rownames(dt_meta) <- NULL
    phy_vst <- phyloseq(otu_table(vst_dt,taxa_are_rows = T),tax_table(phy_fil), set.samp(dt_meta))
    phy_vst
  } else{
    dt_meta <-  data.frame(sample_data(phy_fil))
    dt_meta$SampleID  <- rownames(dt_meta)
    dt_meta <- dt_meta[match(sample_names(phy_fil),dt_meta$SampleID),]
    #make phyloseq object 
    dt_meta$sample  <- rownames(dt_meta)
    rownames(dt_meta) <- NULL
    phy_vst <- transform_sample_counts(phy_fil, function(x) x/sum(x))
    vst_dt <- otu_table(phy_vst)
  }
  
  if (o_glom == T){
    TTABLE <- as.data.frame(tax_table(phy_vst))
    TTABLE$evalue <- NULL
    TTABLE$pident <- NULL
    TMP <- TTABLE$Kingdom
    TTABLE[,which(names(TTABLE)[1:ncol(TTABLE)]!=glom_level)] <- NULL
    TTABLE <- cbind(TMP, TTABLE)
    lnames<-c(names(TMP), names(TTABLE))
    TTABLE <- tax_table(TTABLE)
    rownames(TTABLE) <- taxa_names(phy_vst)
    colnames(TTABLE) <- lnames
    tax_table(phy_vst) <- TTABLE
    phy_vst <- tax_glom(phy_vst, taxrank = glom_level)
    taxa_names(phy_vst) <- tax_table(phy_vst)[,glom_level]
    vst_dt <- otu_table(phy_vst)
  }
  
  X_mic <- data.frame(t(vst_dt))
  t_data_all <- cbind(X_mic, Y_class_all = ifelse(dt_meta$Severity == "severe","Y","N"))
  
  vst_list <- list(phy_vst,t_data_all)
  names(vst_list) <- c("phy_vst","rf_mat")
  return(vst_list)
}

# Import phyloseq object for TNG and STL samples
# Complete observations

# choose taxonomic_level

# TNG samples
phy_tng <-  readRDS("./phy_tng_imp_w_antibiotics-classes_clinical_checked.rds")

# Use the blast assigned name along with SV ID
taxa_names(phy_tng) <- paste0(taxa_names(phy_tng),"_",make.names(as.character(tax_table(phy_tng)[,"Species"])))

# keep only bacterial species
phy_tng
tng_tk <- taxa_names(phy_tng)[which(tax_table(phy_tng)[,3]=="Bacteria")]
phy_tng <- prune_taxa(tng_tk, phy_tng)
phy_tng

list_vst_tng_ASV_relative  <- get_vst_data(phy_tng, o_relative = T, o_glom = F, glom_level = "Genus")
list_vst_tng_Species_relative  <- get_vst_data(phy_tng, o_relative = T, o_glom = T, glom_level = "Species")
list_vst_tng_Genus_relative  <- get_vst_data(phy_tng, o_relative = T, o_glom = T, glom_level = "Genus")
list_vst_tng_Family_relative  <- get_vst_data(phy_tng, o_relative = T, o_glom = T, glom_level = "Family")

phy_vst_tng <- list_vst_tng_ASV_relative[["phy_vst"]]
mic_data_tng_ASV <- list_vst_tng_ASV_relative[["rf_mat"]]
mic_data_tng_Species <- list_vst_tng_Species_relative[["rf_mat"]]
mic_data_tng_Genus <- list_vst_tng_Genus_relative[["rf_mat"]]
mic_data_tng_Family <- list_vst_tng_Family_relative[["rf_mat"]]

# aggregate sets at different taxonomic levels
#mic_data_tng <- cbind(mic_data_tng_ASV[,"Y_class_all"],
#                      mic_data_tng_ASV[,2:ncol(mic_data_tng_ASV)-1], 
#                      mic_data_tng_Species[,2:ncol(mic_data_tng_Species)-1],
#                      mic_data_tng_Genus[,2:ncol(mic_data_tng_Genus)-1],
#                      mic_data_tng_Family[,2:ncol(mic_data_tng_Family)-1])

mic_data_tng <- cbind(mic_data_tng_ASV[,"Y_class_all"],
                      mic_data_tng_ASV[,2:ncol(mic_data_tng_ASV)-1])
names(mic_data_tng)[1] <- "class"

# Now combine sample data 
samp_tng <-  data.frame(sample_data(phy_vst_tng))
rownames(samp_tng)
rownames(mic_data_tng)
samp_tng$sample_type <- NULL
samp_tng$patient_id <- NULL
samp_tng$SampleID <-  NULL
samp_tng$Severity <-  NULL

str(samp_tng)
t_data_tng <- cbind(samp_tng,mic_data_tng)
# Lets see how many rows gets removed if we remove NAs
t_data_tng_comp <- t_data_tng[complete.cases(t_data_tng),]
# Also drop Fatality
t_data_tng$Fatality <- NULL
t_data_tng$Fatality_new <- NULL
t_data_tng$Height <- NULL

t_data_tng_comp$Fatality <- NULL
t_data_tng_comp$Fatality_new  <- NULL
t_data_tng_comp$Height <- NULL

# STL samples
phy_stl <-  readRDS("../../data/phy_stl_imp_w_antibiotics-classes_clinical_checked.rds")

# Use the blast assigned name along with SV ID
taxa_names(phy_stl) <- paste0(taxa_names(phy_stl),"_",make.names(as.character(tax_table(phy_stl)[,"Species"])))
phy_stl
stl_tk <- taxa_names(phy_stl)[which(tax_table(phy_stl)[,3]=="Bacteria")]
phy_stl <- prune_taxa(stl_tk, phy_stl)
phy_stl

list_vst_stl_ASV_relative  <- get_vst_data(phy_stl, o_relative = T, o_glom = F, glom_level = "Genus")
list_vst_stl_Species_relative  <- get_vst_data(phy_stl, o_relative = T, o_glom = T, glom_level = "Species")
list_vst_stl_Genus_relative  <- get_vst_data(phy_stl, o_relative = T, o_glom = T, glom_level = "Genus")
list_vst_stl_Family_relative  <- get_vst_data(phy_stl, o_relative = T, o_glom = T, glom_level = "Family")

phy_vst_stl <- list_vst_stl_ASV_relative[["phy_vst"]]
mic_data_stl_ASV <- list_vst_stl_ASV_relative[["rf_mat"]]
mic_data_stl_Species <- list_vst_stl_Species_relative[["rf_mat"]]
mic_data_stl_Genus <- list_vst_stl_Genus_relative[["rf_mat"]]
mic_data_stl_Family <- list_vst_stl_Family_relative[["rf_mat"]]

# aggregate sets at different taxonomic levels
#mic_data_stl <- cbind(mic_data_stl_ASV[,"Y_class_all"],
#                      mic_data_stl_ASV[,2:ncol(mic_data_stl_ASV)-1], 
#                      mic_data_stl_Species[,2:ncol(mic_data_stl_Species)-1],
#                      mic_data_stl_Genus[,2:ncol(mic_data_stl_Genus)-1],
#                      mic_data_stl_Family[,2:ncol(mic_data_stl_Family)-1])

mic_data_stl <- cbind(mic_data_stl_ASV[,"Y_class_all"],
                      mic_data_stl_ASV[,2:ncol(mic_data_stl_ASV)-1])
names(mic_data_stl)[1] <- "class"
mic_data_stl_Species <- cbind(mic_data_stl_Species[,"Y_class_all"],
                              mic_data_stl_Species[,2:ncol(mic_data_stl_Species)-1])
names(mic_data_stl_Species)[1] <- "class"
mic_data_stl_Genus <- cbind(mic_data_stl_Genus[,"Y_class_all"],
                            mic_data_stl_Genus[,2:ncol(mic_data_stl_Genus)-1])
names(mic_data_stl_Genus)[1] <- "class"

# Now combine sample data 
samp_stl <-  data.frame(sample_data(phy_vst_stl))
rownames(samp_stl)
rownames(mic_data_stl)
samp_stl$sample_type <- NULL
samp_stl$patient_id <- NULL
samp_stl$SampleID <-  NULL
samp_stl$Severity <-  NULL

str(samp_stl)
t_data_stl <- cbind(samp_stl,mic_data_stl)
# Lets see how many rows gets removed if we remove NAs
t_data_stl_comp <- t_data_stl[complete.cases(t_data_stl),]

t_data_stl$Fatality <- NULL
t_data_stl$Fatality_new <- NULL
t_data_stl$Height <- NULL

t_data_stl_comp$Fatality <- NULL
t_data_stl_comp$Fatality_new  <- NULL
t_data_stl_comp$Height <- NULL

# Now combine sample data from TNG and STL
# Grep non SVs
cc_dt_stl <- t_data_stl_comp[,1:grep("class",names(t_data_stl_comp))]
cc_dt_tng <- t_data_tng_comp[,1:grep("class",names(t_data_tng_comp))]
cc_dt_all <-  rbind(cc_dt_stl,cc_dt_tng)
cc_dt_all$patient_id <- rownames(cc_dt_all)
cc_dt_all$patient_id <- gsub("STL|TNG","",cc_dt_all$patient_id)
samp_comb <- unique(cc_dt_all)
rownames(samp_comb) <- samp_comb$patient_id
samp_comb$patient_id <- NULL

# cc and microbiome for STL Species
t_data_stl_Species <- cbind(samp_stl,mic_data_stl_Species)
t_data_stl_Species <- t_data_stl_Species[complete.cases(t_data_stl_Species),]
t_data_stl_Species$Fatality <- NULL
t_data_stl_Species$Fatality_new <- NULL
t_data_stl_Species$Height <- NULL

t_data_stl_Genus <- cbind(samp_stl,mic_data_stl_Genus)
t_data_stl_Genus <- t_data_stl_Genus[complete.cases(t_data_stl_Genus),]
t_data_stl_Genus$Fatality <- NULL
t_data_stl_Genus$Fatality_new <- NULL
t_data_stl_Genus$Height <- NULL

t_data_stl_Family <- cbind(samp_stl,mic_data_stl_Family)
t_data_stl_Family <- t_data_stl_Family[complete.cases(t_data_stl_Family),]
t_data_stl_Family$Fatality <- NULL
t_data_stl_Family$Fatality_new <- NULL
t_data_stl_Family$Height <- NULL

#data_list <- list(mic_data_stl, t_data_stl_comp, mic_data_stl_Species, t_data_stl_Species, mic_data_tng, t_data_tng_comp)
#names(data_list) <- c("STL_ASV","CC_STL_ASV","STL_Species","CC_STL_Species","TNG_ASV","CC_TNG_ASV")

data_list <- list(mic_data_stl, t_data_stl_comp, mic_data_stl_Species, t_data_stl_Species, mic_data_stl_Genus, t_data_stl_Genus, mic_data_stl_Family, t_data_stl_Family)
names(data_list) <- c("STL_ASV","CC_STL_ASV","STL_Species","CC_STL_Species","STL_Genus","CC_STL_Genus","STL_Family","CC_STL_Family")


f1_list<-list()
cfm_list <- list()
roc_dt_list <- list()
importance_list <- list()
lime_list <- list()

###### STL microbiome ASVs only #####################
ilist <- 1
dat_to_use <- data_list[[ilist]]
dat_to_use
dat_to_use$class[dat_to_use$class=="Y"] <- "1"
dat_to_use$class[dat_to_use$class=="N"] <- "0"
# run the severity with microbiome
# first run the boruta using LOO CV
mlo <- ml_data$new()
mlo$fulldata <- dat_to_use
mlo$fulldata
folds<- vfold_cv(mlo$fulldata, v=nrow(mlo$fulldata),repeats=1) 
vimp_list<-list()
len<-1
mlo_boruta_cv_list <- list()
for(len in 1:nrow(folds)){
  mlo_tmp <- mlo
  mlo_tmp$do_rsample_split_fulldata(v_in = nrow(mlo_tmp$fulldata), repeats_in = 1, fold_id = len)
  mlo_tmp$run_boruta(myseed = 100, doTrace = T, maxRuns = 10, opt_encoding = F, is_splitted_data = T)
  mlo_tmp$boruta_importance
  mlo_boruta_cv_list[[len]] <- mlo_tmp
  vimp_list[[len]] <- mlo_tmp$boruta_importance
}
sel_species <-unique(unlist(vimp_list))

# after getting Boruta feature selection run LOO CV for model accuracy estimation
mlo <- ml_data$new()
mlo$fulldata <- dat_to_use
mlo$fulldata
len <- 1
f1_score_list_cv <- list()
lime_list_cv <- list()
for(len in 1:nrow(folds)){
  split<-folds$splits[[len]]
  mlo_tmp <- mlo
  mlo_tmp$fulldata <- mlo_tmp$fulldata[,c("class",sel_species)]
  mlo_tmp$do_rsample_split_fulldata(v_in = nrow(mlo_tmp$fulldata), repeats_in = 1, fold_id = len)
  mlo_tmp$run_rf_classification(myseed = 100, ntrees = 8000, ncores = 20, opt_encoding = F, is_splitted_data = T, o_run_pimp = F)
  f1_score_list_cv[[len]] <- mlo_tmp$rfc_f1
  lime_list_cv[[len]] <- mlo_tmp$rfc_lime_explanation
}  
# F1 score and R0C
f1_dt <-  do.call("rbind",f1_score_list_cv)
f1_dt$data <- names(data_list)[ilist]
f1_list[[ilist]] <- f1_dt
df_f1 <- do.call('rbind',f1_list)
write.csv(f1_dt, paste0(fig_folder,"/f1_",names(data_list)[ilist],".csv"))
lime_list[[ilist]] <- lime_list_cv
lime_list[[ilist]]$name <- names(data_list)[ilist]

roc_var_dt <- roc(df_f1$ytrue,df_f1$ypred,plot=TRUE,smooth = F)
roc_dt <- data.frame(
  tpp=roc_var_dt$sensitivities*100, ## tpp = true positive percentage
  fpp=(1 - roc_var_dt$specificities)*100, ## fpp = false positive precentage
  data = df_f1,
  AUC=as.numeric(gsub("Are.*:","",roc_var_dt$auc)))
roc_dt$variable <- names(data_list)[ilist]
roc_dt_list[[ilist]] <- roc_dt
  
p_roc <- ggplot(roc_dt)+
  geom_path(aes(x= fpp,y = tpp),alpha = 1,size = 1)+
  geom_text(aes(x= fpp,y = tpp,label = AUC))+
  theme_base()
p_roc

f1_score <- as.data.frame(F1_Score(y_pred =f1_dt$ypred, y_true =f1_dt$ytrue,positive = 1))
names(f1_score) <-"F1"
conf_rf <- confusionMatrix(as.factor(f1_dt$ypred),as.factor(f1_dt$ytrue), positive = "1")
conf_rf$data_name <- names(data_list)[ilist]
f1_score$data_name <- names(data_list)[ilist]
f1_score_list[[ilist]] <- f1_score
cfm_list[[ilist]] <- conf_rf

# run the full final model
# Now run random forest model on full data for importance display
mlo_all_data <- ml_data$new()
mlo_all_data$fulldata <- dat_to_use
mlo_all_data$fulldata
mlo_all_data$fulldata <- mlo_all_data$fulldata[,c("class",sel_species)]
mlo_all_data$train_data <- mlo_all_data$fulldata
mlo_all_data$run_rf_classification(myseed = 100, ntrees = 10000, ncores = 10, opt_encoding = F, is_splitted_data = F, o_run_pimp = F)
mlo_all_data$rfc_model$importance
importance_tmp <- as.data.frame(mlo_all_data$rfc_model$importance)
importance_tmp$data_name <- names(data_list)[ilist]
importance_list[[ilist]] <- importance_tmp

###### STL microbiome ASVs + CC #####################
ilist <- 2
dat_to_use <- data_list[[ilist]]
dat_to_use
dat_to_use$class[dat_to_use$class=="Y"] <- "1"
dat_to_use$class[dat_to_use$class=="N"] <- "0"
# run the severity with microbiome
# first run the boruta using LOO CV
mlo <- ml_data$new()
mlo$fulldata <- dat_to_use
mlo$fulldata
folds<- vfold_cv(mlo$fulldata, v=nrow(mlo$fulldata),repeats=1) 
vimp_list<-list()
len<-1
mlo_boruta_cv_list <- list()
for(len in 1:nrow(folds)){
  mlo_tmp <- mlo
  mlo_tmp$do_rsample_split_fulldata(v_in = nrow(mlo_tmp$fulldata), repeats_in = 1, fold_id = len)
  mlo_tmp$run_boruta(myseed = 100, doTrace = T, maxRuns = 100, opt_encoding = F, is_splitted_data = T)
  mlo_tmp$boruta_importance
  mlo_boruta_cv_list[[len]] <- mlo_tmp
  vimp_list[[len]] <- mlo_tmp$boruta_importance
}
sel_species <-unique(unlist(vimp_list))

# after getting Boruta feature selection run LOO CV for model accuracy estimation
mlo <- ml_data$new()
mlo$fulldata <- dat_to_use
mlo$fulldata
len <- 1
f1_score_list_cv <- list()
lime_list_cv <- list()
for(len in 1:nrow(folds)){
  split<-folds$splits[[len]]
  mlo_tmp <- mlo
  mlo_tmp$fulldata <- mlo_tmp$fulldata[,c("class",sel_species)]
  mlo_tmp$do_rsample_split_fulldata(v_in = nrow(mlo_tmp$fulldata), repeats_in = 1, fold_id = len)
  mlo_tmp$run_rf_classification(myseed = 100, ntrees = 5000, ncores = 10, opt_encoding = F, is_splitted_data = T, o_run_pimp = F)
  f1_score_list_cv[[len]] <- mlo_tmp$rfc_f1
  lime_list_cv[[len]] <- mlo_tmp$rfc_lime_explanation
}  
# F1 score and R0C
f1_dt <-  do.call("rbind",f1_score_list_cv)
f1_dt$data <- names(data_list)[ilist]
f1_list[[ilist]] <- f1_dt
df_f1 <- do.call('rbind',f1_list)
write.csv(f1_dt, paste0(fig_folder,"/f1_",names(data_list)[ilist],".csv"))
lime_list[[ilist]] <- lime_list_cv
lime_list[[ilist]]$name <- names(data_list)[ilist]

roc_var_dt <- roc(df_f1$ytrue,df_f1$ypred,plot=TRUE,smooth = F)
roc_dt <- data.frame(
  tpp=roc_var_dt$sensitivities*100, ## tpp = true positive percentage
  fpp=(1 - roc_var_dt$specificities)*100, ## fpp = false positive precentage
  data = df_f1,
  AUC=as.numeric(gsub("Are.*:","",roc_var_dt$auc)))
roc_dt$variable <- names(data_list)[ilist]
roc_dt_list[[ilist]] <- roc_dt

p_roc <- ggplot(roc_dt)+
  geom_path(aes(x= fpp,y = tpp),alpha = 1,size = 1)+
  geom_text(aes(x= fpp,y = tpp,label = AUC))+
  theme_base()
p_roc

f1_score <- as.data.frame(F1_Score(y_pred =f1_dt$ypred, y_true =f1_dt$ytrue,positive = 1))
names(f1_score) <-"F1"
conf_rf <- confusionMatrix(as.factor(f1_dt$ypred),as.factor(f1_dt$ytrue), positive = "1")
conf_rf$data_name <- names(data_list)[ilist]
f1_score$data_name <- names(data_list)[ilist]
f1_score_list[[ilist]] <- f1_score
cfm_list[[ilist]] <- conf_rf

# run the full final model
# Now run random forest model on full data for importance display
mlo_all_data <- ml_data$new()
mlo_all_data$fulldata <- dat_to_use
mlo_all_data$fulldata
mlo_all_data$fulldata <- mlo_all_data$fulldata[,c("class",sel_species)]
mlo_all_data$train_data <- mlo_all_data$fulldata
mlo_all_data$run_rf_classification(myseed = 100, ntrees = 8000, ncores = 10, opt_encoding = F, is_splitted_data = F, o_run_pimp = F)
mlo_all_data$rfc_model$importance
importance_tmp <- as.data.frame(mlo_all_data$rfc_model$importance)
importance_tmp$data_name <- names(data_list)[ilist]
importance_list[[ilist]] <- importance_tmp




###### STL microbiome Species only #####################
ilist <- 3
dat_to_use <- data_list[[ilist]]
dat_to_use
dat_to_use$class[dat_to_use$class=="Y"] <- "1"
dat_to_use$class[dat_to_use$class=="N"] <- "0"
# run the severity with microbiome
# first run the boruta using LOO CV
mlo <- ml_data$new()
mlo$fulldata <- dat_to_use
mlo$fulldata
folds<- vfold_cv(mlo$fulldata, v=nrow(mlo$fulldata),repeats=1) 
vimp_list<-list()
len<-1
mlo_boruta_cv_list <- list()
for(len in 1:nrow(folds)){
  mlo_tmp <- mlo
  mlo_tmp$do_rsample_split_fulldata(v_in = nrow(mlo_tmp$fulldata), repeats_in = 1, fold_id = len)
  mlo_tmp$run_boruta(myseed = 100, doTrace = T, maxRuns = 1000, opt_encoding = F, is_splitted_data = T)
  mlo_tmp$boruta_importance
  mlo_boruta_cv_list[[len]] <- mlo_tmp
  vimp_list[[len]] <- mlo_tmp$boruta_importance
}
sel_species <-unique(unlist(vimp_list))

# after getting Boruta feature selection run LOO CV for model accuracy estimation
mlo <- ml_data$new()
mlo$fulldata <- dat_to_use
mlo$fulldata
len <- 1
f1_score_list_cv <- list()
lime_list_cv <- list()
for(len in 1:nrow(folds)){
  split<-folds$splits[[len]]
  mlo_tmp <- mlo
  mlo_tmp$fulldata <- mlo_tmp$fulldata[,c("class",sel_species)]
  mlo_tmp$do_rsample_split_fulldata(v_in = nrow(mlo_tmp$fulldata), repeats_in = 1, fold_id = len)
  mlo_tmp$run_rf_classification(myseed = 100, ntrees = 8000, ncores = 10, opt_encoding = F, is_splitted_data = T, o_run_pimp = F)
  f1_score_list_cv[[len]] <- mlo_tmp$rfc_f1
  lime_list_cv[[len]] <- mlo_tmp$rfc_lime_explanation
}  
# F1 score and R0C
f1_dt <-  do.call("rbind",f1_score_list_cv)
f1_dt$data <- names(data_list)[ilist]
f1_list[[ilist]] <- f1_dt
df_f1 <- do.call('rbind',f1_list)
write.csv(f1_dt, paste0(fig_folder,"/f1_",names(data_list)[ilist],".csv"))
lime_list[[ilist]] <- lime_list_cv
lime_list[[ilist]]$name <- names(data_list)[ilist]

roc_var_dt <- roc(df_f1$ytrue,df_f1$ypred,plot=TRUE,smooth = F)
roc_dt <- data.frame(
  tpp=roc_var_dt$sensitivities*100, ## tpp = true positive percentage
  fpp=(1 - roc_var_dt$specificities)*100, ## fpp = false positive precentage
  data = df_f1,
  AUC=as.numeric(gsub("Are.*:","",roc_var_dt$auc)))
roc_dt$variable <- names(data_list)[ilist]
roc_dt_list[[ilist]] <- roc_dt

p_roc <- ggplot(roc_dt)+
  geom_path(aes(x= fpp,y = tpp),alpha = 1,size = 1)+
  geom_text(aes(x= fpp,y = tpp,label = AUC))+
  theme_base()
p_roc

f1_score <- as.data.frame(F1_Score(y_pred =f1_dt$ypred, y_true =f1_dt$ytrue,positive = 1))
names(f1_score) <-"F1"
conf_rf <- confusionMatrix(as.factor(f1_dt$ypred),as.factor(f1_dt$ytrue), positive = "1")
conf_rf$data_name <- names(data_list)[ilist]
f1_score$data_name <- names(data_list)[ilist]
f1_score_list[[ilist]] <- f1_score
cfm_list[[ilist]] <- conf_rf

# run the full final model
# Now run random forest model on full data for importance display
mlo_all_data <- ml_data$new()
mlo_all_data$fulldata <- dat_to_use
mlo_all_data$fulldata
mlo_all_data$fulldata <- mlo_all_data$fulldata[,c("class",sel_species)]
mlo_all_data$train_data <- mlo_all_data$fulldata
mlo_all_data$run_rf_classification(myseed = 100, ntrees = 8000, ncores = 10, opt_encoding = F, is_splitted_data = F, o_run_pimp = F)
mlo_all_data$rfc_model$importance
importance_tmp <- as.data.frame(mlo_all_data$rfc_model$importance)
importance_tmp$data_name <- names(data_list)[ilist]
importance_list[[ilist]] <- importance_tmp


###### STL microbiome Species + CC #####################
ilist <- 4
dat_to_use <- data_list[[ilist]]
dat_to_use
dat_to_use$class[dat_to_use$class=="Y"] <- "1"
dat_to_use$class[dat_to_use$class=="N"] <- "0"
# run the severity with microbiome
# first run the boruta using LOO CV
mlo <- ml_data$new()
mlo$fulldata <- dat_to_use
mlo$fulldata
folds<- vfold_cv(mlo$fulldata, v=nrow(mlo$fulldata),repeats=1) 
vimp_list<-list()
len<-1
mlo_boruta_cv_list <- list()
for(len in 1:nrow(folds)){
  mlo_tmp <- mlo
  mlo_tmp$do_rsample_split_fulldata(v_in = nrow(mlo_tmp$fulldata), repeats_in = 1, fold_id = len)
  mlo_tmp$run_boruta(myseed = 100, doTrace = T, maxRuns = 1000, opt_encoding = F, is_splitted_data = T)
  mlo_tmp$boruta_importance
  mlo_boruta_cv_list[[len]] <- mlo_tmp
  vimp_list[[len]] <- mlo_tmp$boruta_importance
}
sel_species <-unique(unlist(vimp_list))

# after getting Boruta feature selection run LOO CV for model accuracy estimation
mlo <- ml_data$new()
mlo$fulldata <- dat_to_use
mlo$fulldata
len <- 1
f1_score_list_cv <- list()
lime_list_cv <- list()
for(len in 1:nrow(folds)){
  split<-folds$splits[[len]]
  mlo_tmp <- mlo
  mlo_tmp$fulldata <- mlo_tmp$fulldata[,c("class",sel_species)]
  mlo_tmp$do_rsample_split_fulldata(v_in = nrow(mlo_tmp$fulldata), repeats_in = 1, fold_id = len)
  mlo_tmp$run_rf_classification(myseed = 100, ntrees = 8000, ncores = 10, opt_encoding = F, is_splitted_data = T, o_run_pimp = F)
  f1_score_list_cv[[len]] <- mlo_tmp$rfc_f1
  lime_list_cv[[len]] <- mlo_tmp$rfc_lime_explanation
}  
# F1 score and R0C
f1_dt <-  do.call("rbind",f1_score_list_cv)
f1_dt$data <- names(data_list)[ilist]
f1_list[[ilist]] <- f1_dt
df_f1 <- do.call('rbind',f1_list)
write.csv(f1_dt, paste0(fig_folder,"/f1_",names(data_list)[ilist],".csv"))
lime_list[[ilist]] <- lime_list_cv
lime_list[[ilist]]$name <- names(data_list)[ilist]

roc_var_dt <- roc(df_f1$ytrue,df_f1$ypred,plot=TRUE,smooth = F)
roc_dt <- data.frame(
  tpp=roc_var_dt$sensitivities*100, ## tpp = true positive percentage
  fpp=(1 - roc_var_dt$specificities)*100, ## fpp = false positive precentage
  data = df_f1,
  AUC=as.numeric(gsub("Are.*:","",roc_var_dt$auc)))
roc_dt$variable <- names(data_list)[ilist]
roc_dt_list[[ilist]] <- roc_dt

p_roc <- ggplot(roc_dt)+
  geom_path(aes(x= fpp,y = tpp),alpha = 1,size = 1)+
  geom_text(aes(x= fpp,y = tpp,label = AUC))+
  theme_base()
p_roc

f1_score <- as.data.frame(F1_Score(y_pred =f1_dt$ypred, y_true =f1_dt$ytrue,positive = 1))
names(f1_score) <-"F1"
conf_rf <- confusionMatrix(as.factor(f1_dt$ypred),as.factor(f1_dt$ytrue), positive = "1")
conf_rf$data_name <- names(data_list)[ilist]
f1_score$data_name <- names(data_list)[ilist]
f1_score_list[[ilist]] <- f1_score
cfm_list[[ilist]] <- conf_rf

# run the full final model
# Now run random forest model on full data for importance display
mlo_all_data <- ml_data$new()
mlo_all_data$fulldata <- dat_to_use
mlo_all_data$fulldata
mlo_all_data$fulldata <- mlo_all_data$fulldata[,c("class",sel_species)]
mlo_all_data$train_data <- mlo_all_data$fulldata
mlo_all_data$run_rf_classification(myseed = 100, ntrees = 8000, ncores = 10, opt_encoding = F, is_splitted_data = F, o_run_pimp = F)
mlo_all_data$rfc_model$importance
importance_tmp <- as.data.frame(mlo_all_data$rfc_model$importance)
importance_tmp$data_name <- names(data_list)[ilist]
importance_list[[ilist]] <- importance_tmp


###### STL microbiome Genus only #####################
ilist <- 5
dat_to_use <- data_list[[ilist]]
dat_to_use
dat_to_use$class[dat_to_use$class=="Y"] <- "1"
dat_to_use$class[dat_to_use$class=="N"] <- "0"
# run the severity with microbiome
# first run the boruta using LOO CV
mlo <- ml_data$new()
mlo$fulldata <- dat_to_use
mlo$fulldata
folds<- vfold_cv(mlo$fulldata, v=nrow(mlo$fulldata),repeats=1) 
vimp_list<-list()
len<-1
mlo_boruta_cv_list <- list()
for(len in 1:nrow(folds)){
  mlo_tmp <- mlo
  mlo_tmp$do_rsample_split_fulldata(v_in = nrow(mlo_tmp$fulldata), repeats_in = 1, fold_id = len)
  mlo_tmp$run_boruta(myseed = 100, doTrace = T, maxRuns = 1000, opt_encoding = F, is_splitted_data = T)
  mlo_tmp$boruta_importance
  mlo_boruta_cv_list[[len]] <- mlo_tmp
  vimp_list[[len]] <- mlo_tmp$boruta_importance
}
sel_species <-unique(unlist(vimp_list))

# after getting Boruta feature selection run LOO CV for model accuracy estimation
mlo <- ml_data$new()
mlo$fulldata <- dat_to_use
mlo$fulldata
len <- 1
f1_score_list_cv <- list()
lime_list_cv <- list()
for(len in 1:nrow(folds)){
  split<-folds$splits[[len]]
  mlo_tmp <- mlo
  mlo_tmp$fulldata <- mlo_tmp$fulldata[,c("class",sel_species)]
  mlo_tmp$do_rsample_split_fulldata(v_in = nrow(mlo_tmp$fulldata), repeats_in = 1, fold_id = len)
  mlo_tmp$run_rf_classification(myseed = 100, ntrees = 10000, ncores = 10, opt_encoding = F, is_splitted_data = T, o_run_pimp = F)
  f1_score_list_cv[[len]] <- mlo_tmp$rfc_f1
  lime_list_cv[[len]] <- mlo_tmp$rfc_lime_explanation
}  
# F1 score and R0C
f1_dt <-  do.call("rbind",f1_score_list_cv)
f1_dt$data <- names(data_list)[ilist]
f1_list[[ilist]] <- f1_dt
df_f1 <- do.call('rbind',f1_list)
write.csv(f1_dt, paste0(fig_folder,"/f1_",names(data_list)[ilist],".csv"))
lime_list[[ilist]] <- lime_list_cv
lime_list[[ilist]]$name <- names(data_list)[ilist]

roc_var_dt <- roc(df_f1$ytrue,df_f1$ypred,plot=TRUE,smooth = F)
roc_dt <- data.frame(
  tpp=roc_var_dt$sensitivities*100, ## tpp = true positive percentage
  fpp=(1 - roc_var_dt$specificities)*100, ## fpp = false positive precentage
  data = df_f1,
  AUC=as.numeric(gsub("Are.*:","",roc_var_dt$auc)))
roc_dt$variable <- names(data_list)[ilist]
roc_dt_list[[ilist]] <- roc_dt

p_roc <- ggplot(roc_dt)+
  geom_path(aes(x= fpp,y = tpp),alpha = 1,size = 1)+
  geom_text(aes(x= fpp,y = tpp,label = AUC))+
  theme_base()
p_roc

f1_score <- as.data.frame(F1_Score(y_pred =f1_dt$ypred, y_true =f1_dt$ytrue,positive = 1))
names(f1_score) <-"F1"
conf_rf <- confusionMatrix(as.factor(f1_dt$ypred),as.factor(f1_dt$ytrue), positive = "1")
conf_rf$data_name <- names(data_list)[ilist]
f1_score$data_name <- names(data_list)[ilist]
f1_score_list[[ilist]] <- f1_score
cfm_list[[ilist]] <- conf_rf

# run the full final model
# Now run random forest model on full data for importance display
mlo_all_data <- ml_data$new()
mlo_all_data$fulldata <- dat_to_use
mlo_all_data$fulldata
mlo_all_data$fulldata <- mlo_all_data$fulldata[,c("class",sel_species)]
mlo_all_data$train_data <- mlo_all_data$fulldata
mlo_all_data$run_rf_classification(myseed = 100, ntrees = 10000, ncores = 10, opt_encoding = F, is_splitted_data = F, o_run_pimp = F)
mlo_all_data$rfc_model$importance
importance_tmp <- as.data.frame(mlo_all_data$rfc_model$importance)
importance_tmp$data_name <- names(data_list)[ilist]
importance_list[[ilist]] <- importance_tmp


###### STL microbiome Genus + CC #####################
ilist <- 6
dat_to_use <- data_list[[ilist]]
dat_to_use
dat_to_use$class[dat_to_use$class=="Y"] <- "1"
dat_to_use$class[dat_to_use$class=="N"] <- "0"
# run the severity with microbiome
# first run the boruta using LOO CV
mlo <- ml_data$new()
mlo$fulldata <- dat_to_use
mlo$fulldata
folds<- vfold_cv(mlo$fulldata, v=nrow(mlo$fulldata),repeats=1) 
vimp_list<-list()
len<-1
mlo_boruta_cv_list <- list()
for(len in 1:nrow(folds)){
  mlo_tmp <- mlo
  mlo_tmp$do_rsample_split_fulldata(v_in = nrow(mlo_tmp$fulldata), repeats_in = 1, fold_id = len)
  mlo_tmp$run_boruta(myseed = 100, doTrace = T, maxRuns = 1000, opt_encoding = F, is_splitted_data = T)
  mlo_tmp$boruta_importance
  mlo_boruta_cv_list[[len]] <- mlo_tmp
  vimp_list[[len]] <- mlo_tmp$boruta_importance
}
sel_species <-unique(unlist(vimp_list))

# after getting Boruta feature selection run LOO CV for model accuracy estimation
mlo <- ml_data$new()
mlo$fulldata <- dat_to_use
mlo$fulldata
len <- 1
f1_score_list_cv <- list()
lime_list_cv <- list()
for(len in 1:nrow(folds)){
  split<-folds$splits[[len]]
  mlo_tmp <- mlo
  mlo_tmp$fulldata <- mlo_tmp$fulldata[,c("class",sel_species)]
  mlo_tmp$do_rsample_split_fulldata(v_in = nrow(mlo_tmp$fulldata), repeats_in = 1, fold_id = len)
  mlo_tmp$run_rf_classification(myseed = 100, ntrees = 10000, ncores = 10, opt_encoding = F, is_splitted_data = T, o_run_pimp = F)
  f1_score_list_cv[[len]] <- mlo_tmp$rfc_f1
  lime_list_cv[[len]] <- mlo_tmp$rfc_lime_explanation
}  
# F1 score and R0C
f1_dt <-  do.call("rbind",f1_score_list_cv)
f1_dt$data <- names(data_list)[ilist]
f1_list[[ilist]] <- f1_dt
df_f1 <- do.call('rbind',f1_list)
write.csv(f1_dt, paste0(fig_folder,"/f1_",names(data_list)[ilist],".csv"))
lime_list[[ilist]] <- lime_list_cv
lime_list[[ilist]]$name <- names(data_list)[ilist]

roc_var_dt <- roc(df_f1$ytrue,df_f1$ypred,plot=TRUE,smooth = F)
roc_dt <- data.frame(
  tpp=roc_var_dt$sensitivities*100, ## tpp = true positive percentage
  fpp=(1 - roc_var_dt$specificities)*100, ## fpp = false positive precentage
  data = df_f1,
  AUC=as.numeric(gsub("Are.*:","",roc_var_dt$auc)))
roc_dt$variable <- names(data_list)[ilist]
roc_dt_list[[ilist]] <- roc_dt

p_roc <- ggplot(roc_dt)+
  geom_path(aes(x= fpp,y = tpp),alpha = 1,size = 1)+
  geom_text(aes(x= fpp,y = tpp,label = AUC))+
  theme_base()
p_roc

f1_score <- as.data.frame(F1_Score(y_pred =f1_dt$ypred, y_true =f1_dt$ytrue,positive = 1))
names(f1_score) <-"F1"
conf_rf <- confusionMatrix(as.factor(f1_dt$ypred),as.factor(f1_dt$ytrue), positive = "1")
conf_rf$data_name <- names(data_list)[ilist]
f1_score$data_name <- names(data_list)[ilist]
f1_score_list[[ilist]] <- f1_score
cfm_list[[ilist]] <- conf_rf

# run the full final model
# Now run random forest model on full data for importance display
mlo_all_data <- ml_data$new()
mlo_all_data$fulldata <- dat_to_use
mlo_all_data$fulldata
mlo_all_data$fulldata <- mlo_all_data$fulldata[,c("class",sel_species)]
mlo_all_data$train_data <- mlo_all_data$fulldata
mlo_all_data$run_rf_classification(myseed = 100, ntrees = 10000, ncores = 10, opt_encoding = F, is_splitted_data = F, o_run_pimp = F)
mlo_all_data$rfc_model$importance
importance_tmp <- as.data.frame(mlo_all_data$rfc_model$importance)
importance_tmp$data_name <- names(data_list)[ilist]
importance_list[[ilist]] <- importance_tmp


















###### STL microbiome Family only #####################
ilist <- 7
dat_to_use <- data_list[[ilist]]
dat_to_use
dat_to_use$class[dat_to_use$class=="Y"] <- "1"
dat_to_use$class[dat_to_use$class=="N"] <- "0"
# run the severity with microbiome
# first run the boruta using LOO CV
mlo <- ml_data$new()
mlo$fulldata <- dat_to_use
mlo$fulldata
folds<- vfold_cv(mlo$fulldata, v=nrow(mlo$fulldata),repeats=1) 
vimp_list<-list()
len<-1
mlo_boruta_cv_list <- list()
for(len in 1:nrow(folds)){
  mlo_tmp <- mlo
  mlo_tmp$do_rsample_split_fulldata(v_in = nrow(mlo_tmp$fulldata), repeats_in = 1, fold_id = len)
  mlo_tmp$run_boruta(myseed = 100, doTrace = T, maxRuns = 1000, opt_encoding = F, is_splitted_data = T)
  mlo_tmp$boruta_importance
  mlo_boruta_cv_list[[len]] <- mlo_tmp
  vimp_list[[len]] <- mlo_tmp$boruta_importance
}
sel_species <-unique(unlist(vimp_list))

# after getting Boruta feature selection run LOO CV for model accuracy estimation
mlo <- ml_data$new()
mlo$fulldata <- dat_to_use
mlo$fulldata
len <- 1
f1_score_list_cv <- list()
lime_list_cv <- list()
for(len in 1:nrow(folds)){
  split<-folds$splits[[len]]
  mlo_tmp <- mlo
  mlo_tmp$fulldata <- mlo_tmp$fulldata[,c("class",sel_species)]
  mlo_tmp$do_rsample_split_fulldata(v_in = nrow(mlo_tmp$fulldata), repeats_in = 1, fold_id = len)
  mlo_tmp$run_rf_classification(myseed = 100, ntrees = 10000, ncores = 10, opt_encoding = F, is_splitted_data = T, o_run_pimp = F)
  f1_score_list_cv[[len]] <- mlo_tmp$rfc_f1
  lime_list_cv[[len]] <- mlo_tmp$rfc_lime_explanation
}  
# F1 score and R0C
f1_dt <-  do.call("rbind",f1_score_list_cv)
f1_dt$data <- names(data_list)[ilist]
f1_list[[ilist]] <- f1_dt
df_f1 <- do.call('rbind',f1_list)
write.csv(f1_dt, paste0(fig_folder,"/f1_",names(data_list)[ilist],".csv"))
lime_list[[ilist]] <- lime_list_cv
lime_list[[ilist]]$name <- names(data_list)[ilist]

roc_var_dt <- roc(df_f1$ytrue,df_f1$ypred,plot=TRUE,smooth = F)
roc_dt <- data.frame(
  tpp=roc_var_dt$sensitivities*100, ## tpp = true positive percentage
  fpp=(1 - roc_var_dt$specificities)*100, ## fpp = false positive precentage
  data = df_f1,
  AUC=as.numeric(gsub("Are.*:","",roc_var_dt$auc)))
roc_dt$variable <- names(data_list)[ilist]
roc_dt_list[[ilist]] <- roc_dt

p_roc <- ggplot(roc_dt)+
  geom_path(aes(x= fpp,y = tpp),alpha = 1,size = 1)+
  geom_text(aes(x= fpp,y = tpp,label = AUC))+
  theme_base()
p_roc

f1_score <- as.data.frame(F1_Score(y_pred =f1_dt$ypred, y_true =f1_dt$ytrue,positive = 1))
names(f1_score) <-"F1"
conf_rf <- confusionMatrix(as.factor(f1_dt$ypred),as.factor(f1_dt$ytrue), positive = "1")
conf_rf$data_name <- names(data_list)[ilist]
f1_score$data_name <- names(data_list)[ilist]
f1_score_list[[ilist]] <- f1_score
cfm_list[[ilist]] <- conf_rf

# run the full final model
# Now run random forest model on full data for importance display
mlo_all_data <- ml_data$new()
mlo_all_data$fulldata <- dat_to_use
mlo_all_data$fulldata
mlo_all_data$fulldata <- mlo_all_data$fulldata[,c("class",sel_species)]
mlo_all_data$train_data <- mlo_all_data$fulldata
mlo_all_data$run_rf_classification(myseed = 100, ntrees = 10000, ncores = 10, opt_encoding = F, is_splitted_data = F, o_run_pimp = F)
mlo_all_data$rfc_model$importance
importance_tmp <- as.data.frame(mlo_all_data$rfc_model$importance)
importance_tmp$data_name <- names(data_list)[ilist]
importance_list[[ilist]] <- importance_tmp


###### STL microbiome Family + CC #####################
ilist <- 8
dat_to_use <- data_list[[ilist]]
dat_to_use
dat_to_use$class[dat_to_use$class=="Y"] <- "1"
dat_to_use$class[dat_to_use$class=="N"] <- "0"
# run the severity with microbiome
# first run the boruta using LOO CV
mlo <- ml_data$new()
mlo$fulldata <- dat_to_use
mlo$fulldata
folds<- vfold_cv(mlo$fulldata, v=nrow(mlo$fulldata),repeats=1) 
vimp_list<-list()
len<-1
mlo_boruta_cv_list <- list()
for(len in 1:nrow(folds)){
  mlo_tmp <- mlo
  mlo_tmp$do_rsample_split_fulldata(v_in = nrow(mlo_tmp$fulldata), repeats_in = 1, fold_id = len)
  mlo_tmp$run_boruta(myseed = 100, doTrace = T, maxRuns = 1000, opt_encoding = F, is_splitted_data = T)
  mlo_tmp$boruta_importance
  mlo_boruta_cv_list[[len]] <- mlo_tmp
  vimp_list[[len]] <- mlo_tmp$boruta_importance
}
sel_species <-unique(unlist(vimp_list))

# after getting Boruta feature selection run LOO CV for model accuracy estimation
mlo <- ml_data$new()
mlo$fulldata <- dat_to_use
mlo$fulldata
len <- 1
f1_score_list_cv <- list()
lime_list_cv <- list()
for(len in 1:nrow(folds)){
  split<-folds$splits[[len]]
  mlo_tmp <- mlo
  mlo_tmp$fulldata <- mlo_tmp$fulldata[,c("class",sel_species)]
  mlo_tmp$do_rsample_split_fulldata(v_in = nrow(mlo_tmp$fulldata), repeats_in = 1, fold_id = len)
  mlo_tmp$run_rf_classification(myseed = 100, ntrees = 10000, ncores = 10, opt_encoding = F, is_splitted_data = T, o_run_pimp = F)
  f1_score_list_cv[[len]] <- mlo_tmp$rfc_f1
  lime_list_cv[[len]] <- mlo_tmp$rfc_lime_explanation
}  
# F1 score and R0C
f1_dt <-  do.call("rbind",f1_score_list_cv)
f1_dt$data <- names(data_list)[ilist]
f1_list[[ilist]] <- f1_dt
df_f1 <- do.call('rbind',f1_list)
write.csv(f1_dt, paste0(fig_folder,"/f1_",names(data_list)[ilist],".csv"))
lime_list[[ilist]] <- lime_list_cv
lime_list[[ilist]]$name <- names(data_list)[ilist]

roc_var_dt <- roc(df_f1$ytrue,df_f1$ypred,plot=TRUE,smooth = F)
roc_dt <- data.frame(
  tpp=roc_var_dt$sensitivities*100, ## tpp = true positive percentage
  fpp=(1 - roc_var_dt$specificities)*100, ## fpp = false positive precentage
  data = df_f1,
  AUC=as.numeric(gsub("Are.*:","",roc_var_dt$auc)))
roc_dt$variable <- names(data_list)[ilist]
roc_dt_list[[ilist]] <- roc_dt

p_roc <- ggplot(roc_dt)+
  geom_path(aes(x= fpp,y = tpp),alpha = 1,size = 1)+
  geom_text(aes(x= fpp,y = tpp,label = AUC))+
  theme_base()
p_roc

f1_score <- as.data.frame(F1_Score(y_pred =f1_dt$ypred, y_true =f1_dt$ytrue,positive = 1))
names(f1_score) <-"F1"
conf_rf <- confusionMatrix(as.factor(f1_dt$ypred),as.factor(f1_dt$ytrue), positive = "1")
conf_rf$data_name <- names(data_list)[ilist]
f1_score$data_name <- names(data_list)[ilist]
f1_score_list[[ilist]] <- f1_score
cfm_list[[ilist]] <- conf_rf

# run the full final model
# Now run random forest model on full data for importance display
mlo_all_data <- ml_data$new()
mlo_all_data$fulldata <- dat_to_use
mlo_all_data$fulldata
mlo_all_data$fulldata <- mlo_all_data$fulldata[,c("class",sel_species)]
mlo_all_data$train_data <- mlo_all_data$fulldata
mlo_all_data$run_rf_classification(myseed = 100, ntrees = 10000, ncores = 10, opt_encoding = F, is_splitted_data = F, o_run_pimp = F)
mlo_all_data$rfc_model$importance
importance_tmp <- as.data.frame(mlo_all_data$rfc_model$importance)
importance_tmp$data_name <- names(data_list)[ilist]
importance_list[[ilist]] <- importance_tmp

















