library(R6)
library(xgboost)
library(Matrix)
library(caret)
library(e1071)
library(gtools)
library(tidyverse)
library(randomForest)
library(ROCR)
library(tensorflow)
library(reticulate)
library(keras)
#use_condaenv(condaenv = "tensorflow_cpu", conda = "/Users/vbucci/anaconda3/bin/conda")
#use_condaenv(condaenv = "tensorflow_cpu", conda = "/data/tools/miniconda3/bin/conda")
#use_condaenv(condaenv = "tensorflow_cpu", conda = "/home/vbucci/anaconda3/bin/conda")

library(vita)
library(plyr)
library(dplyr)
library(caret)
library(yardstick)
library(lime)
library(pROC)
library(ranger)
library(rsample)
library(Boruta)
library(MLmetrics)


PFI = function(X, Y, f){
  score = c()
  FI = c()
  
  #score_orig = f %>% evaluate(
  #  X, Y, verbose = 0)
  
  for (i in 1: ncol(X)){
    xi = sample(X[,i], replace = FALSE)
    Xi = cbind(xi, X[,-i])
    
    score[i] = f %>% evaluate(
      Xi, Y, verbose = 0)

    #FI [i] = score[i]/score_orig
    FI [i] = score[i]
  }
  PFI_final = data.frame("Features" = colnames(X), "FI" = FI)
  return(PFI_final[order(PFI_final$Features, decreasing = TRUE),])
}

ml_data <- R6Class("ml_data",
                  public = list(
                    all_frames = NULL,
                    fulldata = NULL,
                    xdata = NULL,
                    ydata = NULL,
                    ptype = NULL,
                    train_data = NULL,
                    test_data = NULL,
                    
                    # boruta
                    boruta_model = NULL,
                    boruta_importance = NULL,
                    
                    # rfc
                    rfc_model = NULL,
                    rfc_importance = NULL,
                    rfc_confusion = NULL, 
                    rfc_roc = NULL, 
                    rfc_f1 = NULL,
                    rfc_lime_explanation = NULL,
                    
                    #xgb
                    xgb_importance = NULL,
                    xgb_confusion = NULL,
                    xgb_roc = NULL,
                    xgb_lime_explanation = NULL, 
                    
                    # nn
                    keras_model = NULL,
                    keras_lime_explanation = NULL,
                    keras_confusion = NULL, 
                    keras_accuracy = NULL,
                    keras_roc = NULL,
                    keras_estimates_tbl = NULL,
                    keras_pfi_importance = NULL,
                    
                    initialize = function(all_frames = NA, 
                                          fulldata = NA, xdata = NA, ydata = NA, ptype = NA) {
                      self$all_frames <- all_frames
                      self$fulldata <- fulldata
                      self$xdata <- xdata
                      self$ydata <- ydata
                      self$ptype <- ptype
                    },
                    
                    load_fulldata_folder = function(indir,c_var) {
                      s_files <- mixedsort(list.files(path = paste0(indir,c_var),pattern = ".csv",full.names = T))
                      # Read all the csv files in a given path and create a column for the source information (Sample =  1,2,3...)
                      all_frames <- 1:length(s_files)  %>%
                        map_dfr(function(x) {
                          read.csv(s_files[x],row.names = 1)%>% mutate(Sample = x ) } )
                      print(all_frames)
                      self$all_frames <- all_frames
                    },
                    
                    load_fulldata = function(batch_id) {
                      dem_dt <-  self$all_frames %>% filter(Sample ==  batch_id)
                      print(dem_dt)
                      dem_dt$Sample <- NULL
                      dem_dt$ID <- NULL
                      self$fulldata <- dem_dt
                    },
                    
                    load_fulldata_list = function(df){
                      dem_dt <- df
                      dem_dt$Sample <- NULL
                      dem_dt$ID <- NULL
                      self$fulldata <- dem_dt
                    },
                    
                    factorize_predictors = function(to_be_factorized) {
                      self$fulldata[to_be_factorized] <- lapply(self$fulldata[to_be_factorized] , factor)
                    },
                    
                    divide_train_and_test = function() {
                      train_dt <- self$fulldata[self$fulldata$Set == "train",]
                      train_dt$Set <-  NULL
                      test_dt <-  self$fulldata[self$fulldata$Set == "test",]
                      test_dt$Set <- NULL
                      self$train_data <- train_dt
                      self$test_data <- test_dt
                    },
                  
                    # run_rf_classification = function(myseed, ntrees, ncores, opt_encoding) {
                    #   
                    #   # one hot encoding for train set
                    #   if (opt_encoding == TRUE){
                    #     # dummify the data (One-hot-encoding)
                    #     dmy <- dummyVars(" ~ .", data = self$train_data)
                    #     train_dt_rf <- data.frame(predict(dmy, newdata = self$train_data))
                    #     train_dt_rf$class <- factor(train_dt_rf$class, levels = c("0","1"))
                    #   }else{
                    #     train_dt_rf <- self$train_data
                    #     train_dt_rf$class <- as.factor(train_dt_rf$class)
                    #   }
                    #   
                    #   # one hot encoding for test set
                    #   if (opt_encoding == TRUE){
                    #     # dummify the data (One-hot-encoding)
                    #     dmy <- dummyVars(" ~ .", data = self$test_data)
                    #     test_dt_rf <- data.frame(predict(dmy, newdata = self$test_data))
                    #     test_dt_rf$class <- factor(test_dt_rf$class,levels = c("0","1"))
                    #   }else{
                    #     test_dt_rf <- self$test_data
                    #     test_dt_rf$class <- as.factor(test_dt_rf$class)
                    #   }
                    #  
                    #   # Random Forest
                    #   # Run RF classification 
                    #   set.seed(myseed)
                    #   rf <- randomForest(class ~.,train_dt_rf, ntree = ntrees, importance = T)
                    #   self$rfc_model <- rf
                    #   print(rf)
                    #   
                    #   # Run permutated importance 
                    #   pimp <- PIMP(train_dt_rf[,-1], train_dt_rf$class,rf, parallel=TRUE, ncores = ncores, seed = myseed)
                    #   p_test <- PimpTest(pimp)
                    #   pimp_all <- data.frame(orig =  p_test$VarImp, p_test$pvalue)
                    #   imp_dt <- pimp_all     
                    #   imp_dt$Predictors <- rownames(imp_dt)
                    #   rownames(imp_dt) <- NULL
                    #   self$rfc_importance<-imp_dt
                    #   
                    #   # get confusion matrix
                    #   p_rf_test <- predict(rf, test_dt_rf,type = "prob")
                    #   pred <-  p_rf_test  %>%
                    #     data.frame() %>%
                    #     mutate(label = test_dt_rf$class,max_prob = max.col(.,"last") -1)
                    #   
                    #   cm_rf <-  confusionMatrix(factor(pred$max_prob), factor(pred$label),positive = "1" )
                    #   print(paste0("RF Accuracy :",cm_rf$overall[1])) 
                    #   self$rfc_confusion <- unlist(cm_rf$byClass)    
                    #   
                    #   # ROC curve values
                    #   if (length(unique(test_dt_rf$class)) < 3){
                    #     print('Num classes < 3 - ROCR')
                    #     # Use ROCR package to plot ROC Curve - Binary Case 0/1
                    #     rf.pred <- prediction(pred$X1, test_dt_rf$class)
                    #     rf.perf <- performance(rf.pred, "tpr", "fpr")
                    #     roc_dt_rf <- data.frame(FPR = unlist(rf.perf@x.values),TPR = unlist(rf.perf@y.values),
                    #                             thres =  unlist(rf.perf@alpha.values), Method =  "RF")
                    #     self$rfc_roc <- roc_dt_rf
                    #   }else{
                    #     print('Num classes > 3 - multiROC (pROC)')
                    #     # USE pROC for 3 or more cases
                    #     p_rf_test_pROC <- predict(rf, test_dt_rf,type = "response")
                    #     roc.multi <- multiclass.roc(as.numeric(test_dt_rf$class), as.numeric(p_rf_test_pROC))
                    #     print(auc(roc.multi))
                    #     self$rfc_roc <- roc.multi
                    #   }
                    # },
                    
                    run_rf_classification = function(myseed, ntrees, ncores, opt_encoding, is_splitted_data, o_run_pimp, o_run_lime) {
                      # debug no run
                      #self <- mlo_tmp
                      #ntrees = 500; ncores = 15; opt_encoding = F; is_splitted_data = T; o_run_pimp = F
                      #myseed = 112
                      
                      # one hot encoding for train set
                      if (opt_encoding == TRUE){
                        # dummify the data (One-hot-encoding)
                        dmy <- dummyVars(" ~ .", data = self$train_data)
                        train_dt_rf <- data.frame(predict(dmy, newdata = self$train_data))
                        train_dt_rf$class <- factor(train_dt_rf$class, levels = c("0","1"))
                      }else{
                        train_dt_rf <- self$train_data
                        train_dt_rf$class <- factor(train_dt_rf$class, levels = c("0","1"))
                      }
                      
                      if (is_splitted_data==T){
                        # one hot encoding for test set
                        if (opt_encoding == TRUE){
                          # dummify the data (One-hot-encoding)
                          dmy <- dummyVars(" ~ .", data = self$test_data)
                          test_dt_rf <- data.frame(predict(dmy, newdata = self$test_data))
                          test_dt_rf$class <- factor(test_dt_rf$class,levels = c("0","1"))
                        }else{
                          test_dt_rf <- self$test_data
                          test_dt_rf$class <- factor(test_dt_rf$class,levels = c("0","1"))
                        }
                      }
                      # Random Forest
                      # Run RF classification 
                      set.seed(myseed)
                      #rf <- randomForest(class ~.,train_dt_rf, ntree = ntrees, importance = T)
                      rf <- ranger(class~.,data=train_dt_rf,importance = "permutation",
                                         probability = T,
                                         num.trees = ntrees,
                                         num.threads = ncores,
                                         replace=FALSE)
                      self$rfc_model <- rf
                      
                      print(rf)
                      print("RF classification training done....")
                      
                      if (o_run_pimp == TRUE) {
                        # Run permutated importance 
                        pimp <- PIMP(train_dt_rf[,-c("class")], train_dt_rf$class, rf, parallel=TRUE, ncores = ncores, seed = myseed)
                        p_test <- PimpTest(pimp)
                        pimp_all <- data.frame(orig =  p_test$VarImp, p_test$pvalue)
                        imp_dt <- pimp_all     
                        imp_dt$Predictors <- rownames(imp_dt)
                        rownames(imp_dt) <- NULL
                        self$rfc_importance<-imp_dt
                      }else {
                        self$rfc_importance <- NULL
                      }
                      
                      if (is_splitted_data == T){
                        p_rf_test <- predict(self$rfc_model, test_dt_rf,type = "response")
                        print(p_rf_test$predictions)
                        #pred <-  p_rf_test  %>%
                        #  data.frame() %>%
                        #  mutate(label = test_dt_rf$class,max_prob = max.col(.,"last") -1)
                        #res_pred = ifelse(pred[,"X1"] >= 0.5,1,0)
                        res_pred = ifelse(p_rf_test$predictions[,"1"] >= 0.5,1,0)
                        print(res_pred)
                        f1_df<-data.frame(pred= res_pred, test= ifelse(test_dt_rf$class == 1,1,0))
                        print(f1_df)
                        self$rfc_f1 <- f1_df
                        
                        print("RF classification prediction done....")
                      
                        # lime explanation
                        if (o_run_lime == T){
                        train_X <-  train_dt_rf[,!colnames(train_dt_rf) %in% "class"]
                        rf <- as_classifier(rf,labels = NULL)
                        print(rf)
                        expln <- lime(train_X, model = rf)
                        print(expln)
                        test_X <-  test_dt_rf[,!colnames(test_dt_rf) %in% "class"]
                        print(test_X)
                        lime_reasons <- lime::explain(x=test_X, explainer=expln,
                                                      n_labels = 1,
                                                      n_features = 4,
                                                      feature_select = "lasso_path")
                        self$rfc_lime_explanation <- lime_reasons
                        }else{
                          self$rfc_lime_explanation <- NA
                        }
                      }
                    },
                    
                    do_rsample_split_fulldata = function(v_in, repeats_in, fold_id){
                      folds<- vfold_cv(self$fulldata, v= v_in, repeats= repeats_in) 
                      split<-folds$splits[[fold_id]]
                      train_dt <- analysis(split)
                      test_dt <- assessment(split)
                      self$train_data <- train_dt
                      self$test_data <- test_dt
                    },
                    
                    run_boruta = function(myseed, doTrace, maxRuns, opt_encoding, is_splitted_data){
                      
                      #lixo = 1/0
                      
                      if (opt_encoding == TRUE){
                        # dummify the data (One-hot-encoding)
                        dmy <- dummyVars(" ~ .", data = self$train_data)
                        train_dt_rf <- data.frame(predict(dmy, newdata = self$train_data))
                        train_dt_rf$class <- factor(train_dt_rf$class, levels = c("0","1"))
                      }else{
                        train_dt_rf <- self$train_data
                        train_dt_rf$class <- as.factor(train_dt_rf$class)
                      }
                      
                      if (is_splitted_data == T){
                        # one hot encoding for test set
                        if (opt_encoding == TRUE){
                          # dummify the data (One-hot-encoding)
                          dmy <- dummyVars(" ~ .", data = self$test_data)
                          test_dt_rf <- data.frame(predict(dmy, newdata = self$test_data))
                          test_dt_rf$class <- factor(test_dt_rf$class,levels = c("0","1"))
                        }else{
                          test_dt_rf <- self$test_data
                          test_dt_rf$class <- as.factor(test_dt_rf$class)
                        }
                      }
                      # Run BORUTA
                      set.seed(myseed)
                      boruta_model <- Boruta(class ~., data=train_dt_rf, doTrace=doTrace, mcAdj = T, maxRuns = maxRuns)
                      self$boruta_model <- boruta_model
                      
                      boruta_signif <- names(boruta_model$finalDecision[boruta_model$finalDecision %in% c("Confirmed","Tentative")])  # collect Confirmed and Tentative variables
                      self$boruta_importance <- boruta_signif
                    },
                    
                    run_xgb_classification = function(myseed, nrounds, ncores){
                      
                      # Create matrix: One-hot-Encoding for factor variables
                      trainm <- sparse.model.matrix(class ~ .-1,data = self$train_data)

                      train_label <- self$train_data[,"class"]
                      train_matrix <-  xgb.DMatrix(data =  as.matrix(trainm), label = train_label)
                      
                      # Same encoding for test matrix
                      testm <- sparse.model.matrix(class ~ .-1,data = self$test_data)
                      test_label <- self$test_data[,"class"]
                      test_matrix <-  xgb.DMatrix(data =  as.matrix(testm), label = test_label) 
                      
                      nc <- length(unique(train_label))
                      xgb_params <-  list("objective" = "multi:softprob",
                                          #"eval.metric"= "logloss",
                                          "num_class" = nc,
                                          # Learning parameter. Controls how much information from a new tree will be used in the Boosting
                                          # Prevents overfitting
                                          # Range is 0 to 1
                                          "eta" = 0.3,
                                          
                                          # look at only few variables to grow each new node in a tree
                                          #"colsample_bylevel" = 0.5,
                                          # subsample ratio of columns when constructing each tree. 
                                          # Subsampling occurs once for every tree constructed.
                                          # colsample_bytree
                                          
                                          # Maximum depth of the trees. Deeper trees have more terminal does and fit more data
                                          # default is 6
                                          "max_depth" = 6,
                                          
                                          # Stochastic boosting. Stochastic boosting uses only a fraction of the data to grow each tree
                                          "subsample" =1 ,
                                          
                                          #Minimum reduction in the loss function required to grow a new node in a tree.
                                          # Ranges from 0 to inf
                                          "gamma" = 0,
                                          # Controls the minimum number of observations in a terminal node
                                          "min_child_weight" = 1,
                                          nthread = ncores)
                      
                      watchlist <-  list(train =  train_matrix, test =  test_matrix)
                      
                      # eXtreme Gradient Boosting Model
                      set.seed(myseed)
                      bst_model <-  xgb.train(params = xgb_params,
                                              data = train_matrix,
                                              nrounds = 500,
                                              watchlist = watchlist,
                                              seed= myseed,verbose = F)
                      
                      self$xgb_importance <- xgb.importance(colnames(train_matrix), model = bst_model)
                      
                      # prediction
                      p <- predict(bst_model, newdata = test_matrix)
                      pred <-  matrix(p, nrow = nc, ncol = length(p)/nc)  %>%
                        t() %>%
                        data.frame() %>%
                        mutate(label = test_label,max_prob = max.col(.,"last") -1)
                      cm_xgb  <- confusionMatrix(factor(pred$max_prob), factor(pred$label), positive = "1" )
                      print(cm_xgb)
                      print(paste0("XgB Accuracy:",cm_xgb$overall[1])) 
                      self$xgb_confusion <- cm_xgb
                      
                      # Use ROCR package to plot ROC Curve
                      xgb.pred <- prediction(pred$X2, test_label)
                      xgb.perf <- performance(xgb.pred, "tpr", "fpr")
                      plot(xgb.perf)
                      roc_dt_xgb <- data.frame(FPR = unlist(xgb.perf@x.values),TPR = unlist(xgb.perf@y.values),
                                               thres =  unlist(xgb.perf@alpha.values),Method = "xGb" )
                      self$xgb_roc <-  roc_dt_xgb
                    },
                    
                    run_keras = function(myseed,center_scale_id_start,nc){
                      
                      #https://rstudio-pubs-static.s3.amazonaws.com/452498_2bb5b64288b94710a86982c3f70bb483.html
                      
                      # one hot encoding for RF
                      # dummify the data (One-hot-encoding)
                      dmy <- dummyVars(" ~ .", data = self$train_data)
                      train_dt_rf <- data.frame(predict(dmy, newdata = self$train_data))
                      train_dt_rf$class <- factor(train_dt_rf$class, levels = c("0","1"))
                      
                      # Same thing for test set
                      dmy <- dummyVars(" ~ .", data = self$test_data)
                      test_dt_rf <- data.frame(predict(dmy, newdata = self$test_data))
                      test_dt_rf$class <- factor(test_dt_rf$class,levels = c("0","1"))
                      
                      X_train <- as.matrix(train_dt_rf[,2:ncol(train_dt_rf)])
                      print(X_train)
                      X_train[,center_scale_id_start:ncol(X_train)] =  scale(X_train[,center_scale_id_start:ncol(X_train)],center = T,scale = T)
                      y_train=to_categorical(as.matrix(train_dt_rf$class))
                      y_train=as.matrix(train_dt_rf$class)
                      X_test=as.matrix(test_dt_rf[,2:ncol(test_dt_rf)])
                      X_test[,center_scale_id_start:ncol(X_test)] =  scale(X_test[,center_scale_id_start:ncol(X_test)],center = T,scale = T)
                      y_test=as.matrix(test_dt_rf$class)
                      
                      model <- keras_model_sequential() # initialize the model
                      use_session_with_seed(myseed)
                      
                      # set the models two hidden states -- this part is ad-hoc needs experimentation (heuristic) depending on the dataset
                      # model   %>%
                      #   layer_dense(units=150,
                      #               activation="relu",
                      #               input_shape=ncol(X_train),
                      #               kernel_initializer=initializer_random_uniform(minval = -0.05, maxval = 0.05, seed = myseed)) 
                      # model %>%  
                      #   layer_dense(units=1,
                      #               activation="softmax",
                      #               kernel_initializer=initializer_random_uniform(minval = -0.05, maxval = 0.05, seed = myseed))
                      # 
                      # model %>%
                      #   compile(loss="binary_crossentropy",
                      #           optimizer=optimizer_sgd(lr=0.001),
                      #           metrics = "accuracy")
                      
                      model %>% 
                        layer_dense(units=60,activation="relu", ncol(X_train),
                                    kernel_initializer=initializer_random_uniform(minval = -0.05, maxval = 0.05, seed = 100)) %>%
                        layer_dropout(0.5) %>%
                        layer_dense(units=40,activation="relu"
                                    ,kernel_initializer=initializer_random_uniform(minval = -0.05, maxval = 0.05, seed = 100)) %>%
                        layer_dense(units=1,activation="sigmoid",
                                    kernel_initializer=initializer_random_uniform(minval = -0.05, maxval = 0.05, seed = 100))
                      
                      compile(model,loss="binary_crossentropy",
                              optimizer=optimizer_sgd(lr=0.01,clipvalue=0.5,clipnorm=1), metrics = "accuracy")
                      
                      model
                      
                      self$keras_model <- model
                      
                      # fit the model to the training data
                      ev.model  =    fit(model,X_train, y_train, 
                                         epochs = 10, 
                                         batch_size = 10,
                                         verbose=1,
                                         validation_data = list(X_test,y_test))
                      
                      # plot loss and accuracy (modify using ggplot next stage)
                      #plot(ev.model)
                      #plot(ev.model$metrics$loss, main="Model Loss", xlab = "epoch", ylab="loss", col="orange", type="l", ylim = c(0,1))
                      #lines(ev.model$metrics$val_loss, col="skyblue")
                      #legend("topright", c("Training","Testing"), col=c("orange", "skyblue"), lty=c(1,1))
                      
                      # predict the test data (classes) 
                      evaluate(model, X_test, y_test, verbose = 1)
                      prediction_classes <- model %>% predict_classes(x = as.matrix(X_test)) %>% as.vector()
                      print(prediction_classes)
                      
                      # predict the test data (probabilities) 
                      prediction_probs <- 
                        predict_proba (object = model, 
                                       x = as.matrix(X_test)) %>%
                        as.vector()                      
                      
                      estimates_keras_tbl <- tibble(
                       truth      = as.factor(y_test),
                       estimate   = as.factor(prediction_classes),
                       class_prob = prediction_probs )

                      estimates_keras_tbl$estimate <- factor(estimates_keras_tbl$estimate, 
                                                             levels = levels(estimates_keras_tbl$truth))
                      print(estimates_keras_tbl)
                      
                      self$keras_estimates_tbl <- estimates_keras_tbl
                      
                      print(confusionMatrix(estimates_keras_tbl$estimate,
                                            estimates_keras_tbl$truth,
                                            positive="1"))
                      
                      self$keras_confusion <- 
                        confusionMatrix(estimates_keras_tbl$estimate,
                                        estimates_keras_tbl$truth,
                                        positive="1")
                      
                      estimates_keras_tbl_accuracy <- estimates_keras_tbl %>% metrics (truth, estimate)
                      print(estimates_keras_tbl_accuracy)
                      self$keras_accuracy <- estimates_keras_tbl_accuracy
                      
                      estimates_keras_tbl_roc <- estimates_keras_tbl %>% roc_auc(truth, class_prob) 
                      self$keras_roc <- estimates_keras_tbl_roc
                      
                      pfi_importance <-PFI(X_test, y_test, model)
                      self$keras_pfi_importance <- pfi_importance
                      
                      # analysis with lime
                      model_type.keras.engine.sequential.Sequential <- function(x, ...) {
                        "classification"}
                      
                      # Setup lime::predict_model()
                      predict_model.keras.engine.sequential.Sequential <- function (x, newdata, type, ...) {
                        pred <- predict(object = x, x = as.matrix(newdata))
                        data.frame (Positive = pred, Negative = 1 - pred) }
                      
                      # Usando lime() no train set
                      #explainer <- lime(x = as.data.frame(X_train), model= model)
                      
                      # Explainer
                      #system.time(
                      #  explanation <- lime::explain (
                      #    x = as.data.frame(X_test),
                      #    explainer = explainer, 
                      #    n_features = ncol(X_train),
                      #    n_labels = 1))
                      #print(explanation)
                      
                      #self$keras_lime_explanation <- explanation
                      
                    },
                    
                    greet = function() {
                      cat(paste0("Hello, my name is ", self$name, ".\n"))
                    }
                  )
)
